# Head First Design Pattern

insights from the pioneer

## chapters

#### chap01 Welcome to Design Patterns
[the strategy pattern](./src/chap01)

#### chap02 Keeping your Objects in the know
the observer pattern

#### chap03 Decorating Objects

#### chap04 Baking with OO Goodness

#### chap05 One of a Kind Objects
[the singleton pattern](./src/chap05)

#### chap06 Encapsulating Invocation

#### chap07 Being Adaptive

#### chap08 Encapsulating Algorithms

#### chap09 Well-Managed Collections

#### chap10 The State of Things

#### chap11 Controlling Objects Access

#### Chap12 Patterns of Patterns

#### Chap13 Patterns in the Real World



## Appendix Extensions

#### JAVA 객체지향 디자인 패턴(Alias : jodp)
[goto](./src/appendix/jodp)

#### UML


## [etc : furthermore](./src/etc)