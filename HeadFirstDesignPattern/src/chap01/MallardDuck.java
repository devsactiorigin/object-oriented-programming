package chap01;

import chap01.EncapsulatedBehavior.FlyWithWings;
import chap01.EncapsulatedBehavior.Quack;
import chap01.EncapsulatedBehavior.ShortStrategy;

public class MallardDuck extends Duck {
 
	public MallardDuck() {
 
		quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
        strategyBehavior = new ShortStrategy();

	}
 
	public void display() {
		System.out.println("I'm a real Mallard duck");
	}
}
