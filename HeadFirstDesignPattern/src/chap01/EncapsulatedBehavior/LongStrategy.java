package chap01.EncapsulatedBehavior;

public class LongStrategy implements StrategyBehavior {

	@Override
	public void SelectingStrategy() {
		// 1은 장기적인 전략선택으로 서로에게 이롭다.
		System.out.println("I choose long strategy");
	}

}
