package chap01.EncapsulatedBehavior;

public class ShortStrategy implements StrategyBehavior {

	@Override
	public void SelectingStrategy() {
		// 0은 단기적인 전략선택으로 결국 서로에게 이롭지 못하다.
		System.out.println("i choose short strategy");
	}

}
