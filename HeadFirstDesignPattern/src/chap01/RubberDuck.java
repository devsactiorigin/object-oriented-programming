package chap01;

import chap01.EncapsulatedBehavior.FlyNoWay;
import chap01.EncapsulatedBehavior.Squeak;

public class RubberDuck extends Duck {
 
	public RubberDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Squeak();
	}
 
	public void display() {
		System.out.println("I'm a rubber duckie");
	}
}
