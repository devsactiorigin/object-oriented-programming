package chap01;

import chap01.EncapsulatedBehavior.FlyWithWings;
import chap01.EncapsulatedBehavior.Quack;

public class RedHeadDuck extends Duck {
 
	public RedHeadDuck() {
		flyBehavior = new FlyWithWings();
		quackBehavior = new Quack();
	}
 
	public void display() {
		System.out.println("I'm a real Red Headed duck");
	}
}
