package chap01;

import chap01.EncapsulatedBehavior.FlyBehavior;
import chap01.EncapsulatedBehavior.QuackBehavior;
import chap01.EncapsulatedBehavior.StrategyBehavior;

public abstract class Duck {
	FlyBehavior flyBehavior;
	QuackBehavior quackBehavior;
	StrategyBehavior strategyBehavior;
	
	public Duck() {
	}
 
	public void setFlyBehavior (FlyBehavior fb) {
		flyBehavior = fb;
	}
 
	public void setQuackBehavior(QuackBehavior qb) {
		quackBehavior = qb;
	}
	
	// new config
	public void setStrategyBehavior(StrategyBehavior ssb) {
		strategyBehavior = ssb;
	}
 
	abstract void display();
 
	public void performFly() {
		flyBehavior.fly();
	}
 
	public void performQuack() {
		quackBehavior.quack();
	}
 
	public void performStrategy() {
		strategyBehavior.SelectingStrategy();
	}
	
	public void swim() {
		System.out.println("All ducks float, even decoys!");
	}
}
