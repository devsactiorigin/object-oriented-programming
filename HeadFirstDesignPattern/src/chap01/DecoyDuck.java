package chap01;

import chap01.EncapsulatedBehavior.FlyNoWay;
import chap01.EncapsulatedBehavior.MuteQuack;

public class DecoyDuck extends Duck {
	public DecoyDuck() {
		setFlyBehavior(new FlyNoWay());
		setQuackBehavior(new MuteQuack());
	}
	public void display() {
		System.out.println("I'm a duck Decoy");
	}
}
