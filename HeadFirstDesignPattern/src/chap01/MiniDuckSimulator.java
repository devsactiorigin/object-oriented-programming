package chap01;

import chap01.EncapsulatedBehavior.FlyRocketPowered;
import chap01.EncapsulatedBehavior.FlyWithWings;

public class MiniDuckSimulator {
 
	public static void main(String[] args) {
 
		MallardDuck	mallard = new MallardDuck();
		RubberDuck	rubberDuckie = new RubberDuck();
		DecoyDuck	decoy = new DecoyDuck();
 
		ModelDuck	model = new ModelDuck();

		mallard.performQuack();
		rubberDuckie.performQuack();
		decoy.performQuack();
		
		System.out.println("--");
		model.performFly();
		// after config the model's FlyBehavior value
		// config way 1
		FlyWithWings reconfigValofFly = new FlyWithWings();
		model.setFlyBehavior(reconfigValofFly);
		model.performFly();
		
		// config way 2
		model.setFlyBehavior(new FlyRocketPowered());
		model.performFly();
		System.out.println("--");
		
	}
}
