package chap01;

import chap01.EncapsulatedBehavior.FlyNoWay;
import chap01.EncapsulatedBehavior.Quack;

public class ModelDuck extends Duck {
	public ModelDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Quack();
	}

	public void display() {
		System.out.println("I'm a model duck");
	}
}
