package chap01;

// ArrayList 사용을 위한 임포트
import java.util.*;

import chap01.EncapsulatedBehavior.FlyNoWay;
import chap01.EncapsulatedBehavior.LongStrategy;
import chap01.EncapsulatedBehavior.Squeak;

public class MiniDuckGameSimulator {

	public static void main(String[] args) {
		
		Scanner scanner=new Scanner(System.in);
		
		// 2 마리의 청둥오리를 품을 ArrayList를 선언한다.
		int cnt_duck=2;
		ArrayList<Duck> Ducks = new ArrayList<Duck>();
		for (int i =0; i<cnt_duck ; i++) {
			//Ducks.add(new Duck());
			// 추상 클래스라서 상단과같이 초기화불과하므로, 아래와같이 다형성(MallardDuck ⊂ Duck)을 이용하는 초기화
			Ducks.add(new MallardDuck());
		}
		// 상단 초기화를 통해, 청둥오리마다 이름은 없더래도 숫자 인덱스 0, 1이 부여되었다고 볼수있다.
		for (int i=0;i<cnt_duck;i++) {
			System.out.println("==================================");
			System.out.println("now new duck will introduce itself");
			System.out.println("==================================");
			
			System.out.println("난 " + i +"번째 청둥오리야");
			// 청둥오리의 기본 메소드들을 호출해본다.
			System.out.println("난 오리(Duck)이란 추상메소드를 상속받았으므로 swim, display할수있고, 청둥오리로서 display를 overriding했어");
			Ducks.get(i).swim();
			Ducks.get(i).display();
			// 사전 계약된 인터페이스들을 사용해보고 변경해본다.
			System.out.println("그리고,");
			System.out.println("나는 FlyBehavior, QuackBehavior Interface와 계약된 상태이고, 각 인터페이스를 상속받은 하위클래스를 선택함으로서 구체적인 계약행위를 선택가능하지");
			System.out.println("-청둥오리인 나는 FlyBehavior 인터페이스의 하위 클래스들 중 FlyWithWings로 세부계약(초기화)되어있어, 그리고 추후 세부계약을 변경가능하지 => ");
			Ducks.get(i).performFly();
			Ducks.get(i).setFlyBehavior(new FlyNoWay()); // 변경
			Ducks.get(i).performFly();
			
			System.out.println("-청둥오리인 나는 QuackBehavior 인터페이스의 하위 클래스들 중 Quack으로 세부계약되어있어, 그리고 추후 세부계약을 변경가능하지");
			Ducks.get(i).performQuack();
			Ducks.get(i).setQuackBehavior(new Squeak()); // 변경
			Ducks.get(i).performQuack();
			System.out.println("이로서 " + i +"번째 청둥오리인 나의 자기소개를 마칠게");
		}
		
		System.out.println("!!!! Alert !!!!");
		System.out.println("평화롭던 2마리의 청둥오리 사이에 문제가 생겼어, 같이 swim하던 물 웅덩이가 줄어들어서 딱 1마리만 들어갈수있게되었거든,");
		System.out.println("평화롭게 돌아가면서 사용하면 문제없지만...두마리가 서로 내가 사용하겠다고 하루 종일 싸우면 그날은 둘다 싸우느라 물웅덩이에 들어가지 못해");
		System.out.println("한편, 청둥오리는 2일연속 물웅덩이에 들어가지 않으면 죽어버리지...4일 동안 2마리가 모두 살도록 이끌어보자");
		
		// i번째 청둥오리의 의사표시를 저장할 배열 선언
		// arr[0]은 0번째 청둥오리의 전략 선택에 매핑되며, 0은 오직 나만 생각하겠다, 1은 상대도 생각하겠다를 의미한다.
		int[] arrStrategy=new int[2];
		
		// i번째 청둥오리의 생명력을 저장할 배열 선언, 2로 시작되어 2일 연속 물웅덩이에 못들어가면 0이되서 죽는다.
		int[] arrLife=new int[2];
		
		// 기본 변수들 초기값 설정, 최악인 상태이다.
		for(int i=0;i<arrLife.length;i++) {
			arrStrategy[i]=0;
			arrLife[i]=2;
		}
		
		System.out.println("now game start...");
		// 4 day
		for(int d=0;d<4;d++) {			
			System.out.println();
			System.out.println("Today is " + (d+1) + "th day");
			System.out.println();
			
			if(d==0) {
				System.out.println("It is First day! look after Ducks' strategies..");
				
				for(int i=0;i<Ducks.size();i++) {
					System.out.print("i am Duck "+i +", ");
					Ducks.get(i).performStrategy();
					System.out.printf("strategy is %d, life is %d",arrStrategy[i],arrLife[i]);
					System.out.println();
				}
			} else if(d>=1) {
				System.out.println("After First day! look after Ducks' strategies..");
				for(int i=0;i<Ducks.size();i++) {
					System.out.print("i am Duck "+i+", ");
					Ducks.get(i).performStrategy();
					System.out.printf("strategy is %d, life is %d",arrStrategy[i],arrLife[i]);
					System.out.println();
				}				
			}
			
			
			// per one day life is discounted
			// 우선 둘다 최악의 선택의 선택을 하는 상황에 대해서만 정확한 처리
			int sum_strategy=0;
			for(int i=0;i<arrStrategy.length;i++) {
				sum_strategy+=arrStrategy[i];
			}
			
			// 서로 양보안하고 싸우다가 체감되는 경우, 추후 리패토링 시 독립되야할 1순위 부분
			if(sum_strategy==0) {
				for(int i=0;i<arrStrategy.length;i++) {
					arrLife[i]-=1;
				}	
			}
			
			System.out.println("plz input 1 only");
			for(int i=0;i<arrStrategy.length;i++) {
				arrStrategy[i]=scanner.nextInt();
				
				Ducks.get(i).setStrategyBehavior(new LongStrategy());
			}
			
			
			
			// 2 마리 중 1마리라도 죽으면 break한다. 추후 업데이트
//			for(int i=0;i<arrStrategy.length;i++) {
//				if(arrStrategy[i]==0) {
//					System.out.println("you fail to best...");
//					breakToken=1;
//				}
//			}
			
		}
	}

}
