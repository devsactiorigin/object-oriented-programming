# overview
login = doSecurity -> authentication -> authorization -> connection

으로 재해석

## explaination

AbstLoginHelper.java ; 규격화된 일련의 작업에 대응하는 추상 상위 클래스

ConcreteLoginHelper1.java ; 추상 클래스를 구체화한 하위 클래스

ConcreteLoginHelper2.java ; 규격화된 일련의 작업은 유지되지만, 하위 작업에 변동된 요구사항에 대응하여 변화된 하위 클래스

Application.java ; 실행 클래스, 드라이브 클래스


# reference
https://github.com/garam-park/java-designpattern