package etc.Chapter03TemplateMethod.app;

import etc.Chapter03TemplateMethod.AbstLoginHelper;
import etc.Chapter03TemplateMethod.ConcreteLoginHelper;
import etc.Chapter03TemplateMethod.ConcreteLoginHelperExtention;

import etc.Chapter03TemplateMethod.NoTemplateLoginHelper;
import etc.Chapter03TemplateMethod.NoTemplateLoginHelper2;


public class Application {

	public static void main(String[] args) {
		// abstract : concrete1
		System.out.println("instance 1 of concrete1");
		AbstLoginHelper abstConnectHelper = new ConcreteLoginHelper();
		
		abstConnectHelper.requestConnection("info");
		
		System.out.println("instance 2 of concrete1");
		AbstLoginHelper abstConnectHelper2 = new ConcreteLoginHelper();

		abstConnectHelper2.requestConnection("info");
		
		System.out.println();
		System.out.println("# Alert !");
		System.out.println("Requirement of sub process is changed");

		// abstract : concrete2 which is variations of concrete1
		System.out.println("instance 1 of concrete2");
		AbstLoginHelper abstConnectHelperExtention = new ConcreteLoginHelperExtention();
		
		abstConnectHelperExtention.requestConnection("info");

		System.out.println();
		System.out.println("# if there is no template....");
		System.out.println("");
		
		// notemplate_concrete1 : notemplate_concrete1
		System.out.println("notemplate_concrete1");
		NoTemplateLoginHelper no = new NoTemplateLoginHelper();
		no.보안("info");
		no.인증("id","password");
		no.인가("charactername");
		no.연결("info");
		
		// notemplate_concrete2 : notemplate_concrete2
		System.out.println("notemplate_concrete2");
		NoTemplateLoginHelper2 no2 = new NoTemplateLoginHelper2();
		no2.복호화단계("info");
		no2.인증단계("id","password");
		no2.인가단계("charactername");
		no2.접속단계("info");
		
		System.out.println();
		System.out.println("# Alert !");
		System.out.println("Requirement of sub process is changed");

		System.out.println("...");
		System.out.println("Sorry..I give up!");
	}

}