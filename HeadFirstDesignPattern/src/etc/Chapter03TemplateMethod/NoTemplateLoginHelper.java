package etc.Chapter03TemplateMethod;

public class NoTemplateLoginHelper{

	public String 보안(String info) {
		System.out.println("step 1 : 보안");
		return info;
	}

	public String 인증(String id, String password) {
		System.out.println("step 2 : 인증");
		if(id.equals("abc") && password.equals("abc"))
			return "true info";
		else
			return "false info";
	}

	public int 인가(String charactername) {
		System.out.println("step 3 : 인가");
		return 0;
	}

	public String 연결(String info) {
		System.out.println("step 4 : 연결");
		return info;
	}
	
}