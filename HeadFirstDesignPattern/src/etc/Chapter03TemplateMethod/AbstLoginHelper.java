package etc.Chapter03TemplateMethod;

public abstract class AbstLoginHelper {
	/*
	 * steps of template method of login
	 * <primitive method>
	 * step1 => primitive method 1 : 보안(복호화) ; doSecurity
	 * step2 => primitive method 2 : 인증 ; authentication
	 * step3 => primitive method 3 : 권한 ; authorization
	 * step4 => primitive method 4 : 접속 ; connection
	 * 
	 * <hook method at 'this time'> ; 시간이 흘러 타당하면 primitive로 봐도 무방
	 * optional step => hook method 1 : 토큰인증 ; token
	 */
	abstract protected String doSecurity(String info);

	abstract protected String authentication(String id, String password);

	abstract protected int authorization(String CharacterName);

	abstract protected String connection(String info);
	
	abstract protected String token(String time);
	
	// template method = sum of steps, sum of (primitive method or hook method),whole method
	// but this requestConnection does not be overided by sub class, it just suggest
	public String requestConnection(String info) {
		/*
		 * decodedInfo : doSecurity를 통해 복호화된 Info
		 * id, password : decodedInfo에서 추출된 id, password로 가정
		 * */
		String decodedInfo, id, password,userInfo,CharacterName;
		
		// 추후 추가된 절차
		String tokenInfo;

		// step1 : 보안(복호화) ; doSecurity
		decodedInfo = doSecurity(info);

		// decodedInfo에서 id 와 password를 추출
		/*
		 * 가령,
		 * id=decodeInfo.get("id");
		 * password=decodeInfo.get("password");
		 * 
		 * 이를 약식으로 아래와같이 표현
		 * */
		id = "abc";
		password = "abc";

		// step2 : 인증 ; authentication
		userInfo = authentication(id, password);
		
		// 입력받은 info 속에 time변수가 있어서, 시간별로 토큰을 발급해준다고 이해가능
		tokenInfo = token(info);

		// step3 : 권한 ; authorization ; userInfo에서 여러개의 캐릭터 중 원하는 CharacterName을 찾아 냅니다.
		CharacterName = "abc";

		int result = authorization(CharacterName);

		switch (result) {

		case 0:// 무료회원
			break;
		case 1:// 유료회원
			break;
		case 2:// 게임 마스터
			break;
		case 3:// 접속 권한 없음
			break;
		default:
			break;
		}
		
		// step4 : 접속 ; connection
		return connection(userInfo);
	}
}