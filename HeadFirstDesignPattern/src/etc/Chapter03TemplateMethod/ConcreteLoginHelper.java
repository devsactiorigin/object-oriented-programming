package etc.Chapter03TemplateMethod;

public class ConcreteLoginHelper extends AbstLoginHelper {

	@Override
	protected String doSecurity(String info) {
		System.out.println("step 1 : doSecurity");
		return info;
	}

	@Override
	protected String authentication(String id, String password) {
		System.out.println("step 2 : authentication");
		if(id.equals("abc") && password.equals("abc"))
			return "true info";
		else
			return "false info";
	}

	@Override
	protected int authorization(String charactername) {
		System.out.println("step 3 : authorization");
		return 0;
	}

	@Override
	protected String connection(String info) {
		System.out.println("step 4 : connection");
		return info;
	}

	
	// 한두개는 그렇다고 해도, hook가 너무 많으면?
	@Override
	protected String token(String info) {
		// TODO Auto-generated method stub
		return null;
	}

}