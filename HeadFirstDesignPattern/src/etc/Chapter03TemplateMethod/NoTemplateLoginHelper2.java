package etc.Chapter03TemplateMethod;

public class NoTemplateLoginHelper2{

	public String 복호화단계(String info) {
		System.out.println("step 1 : 복호화");
		return info;
	}

	public String 인증단계(String id, String password) {
		System.out.println("step 2 : 인증단계");
		if(id.equals("abc") && password.equals("abc"))
			return "true info";
		else
			return "false info";
	}

	public int 인가단계(String charactername) {
		System.out.println("step 3 : 인가단계");
		return 0;
	}

	public String 접속단계(String info) {
		System.out.println("step 4 : 접속단계");
		return info;
	}

}