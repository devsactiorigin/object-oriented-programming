package appendix.jodp.chap11TemplateMethod;

import java.util.*;

public abstract class ReportGenerator {
	/*
	 * Step 1 of Requirement : 전체고객 중의 관심고객을 선별 => *전처리(자료형 정의,...), 판정
	 * */
	
	// primitive method 1.1 : 전처리 차원의 select
	protected List<Customer> select(List<Customer> customers){
		// Step 1.1 : 데이터 전처리로서 자료형 정의 (만약에..Map도 섞여온다면?)
		List<Customer> selected = new ArrayList<Customer>();
		
		// Step 1.2 : 판정
		for (Customer customer:customers) {
			// customerReportCondition에 일치하는 고객만 선택
			if(customerReportCondition(customer)) {
				selected.add(customer);
			}
		}
		return selected;
	}
	
	// primitive method 1.2 : customerReportCondition (of select method) 
	protected abstract boolean customerReportCondition(Customer customer); // Thanks for java

	/*
	 * Step 2 of Requirement : 관심고객 수 계산
	 * */
	
	// primitive method 2 : getReportHeader
	protected abstract String getReportHeader(List<Customer> customers);

	/*
	 * Step 3 of Requirement : 관심고객 분석1_나열
	 * */
	
	// primitive method 3 : getReportForCustomer
	protected abstract String getReportForCustomer(Customer customers);

	/*
	 * Step k of Requirement : 관심고객들 중 특정정보 종합
	 * */
	
	// hook method 1 : getReportFooter + (or primitive method 4, at the point of future)
	protected abstract String getReportFooter(List<Customer> customers);
	
	/*
	 * Grouping Method of Template = Template Method : generate
	 * */
	public String generate(List<Customer> customers) {
		List<Customer> selectedCustomers = select(customers);
		String report = getReportHeader(selectedCustomers);
		
		for(int i = 0 ; i < selectedCustomers.size();i++) {
			Customer customer = selectedCustomers.get(i);
			report += getReportForCustomer(customer);
		}
		
		report += getReportFooter(selectedCustomers);
		
		return report;
	}
}
