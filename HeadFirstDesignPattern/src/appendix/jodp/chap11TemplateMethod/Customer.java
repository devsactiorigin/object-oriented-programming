package appendix.jodp.chap11TemplateMethod;

import lombok.Data;

@Data
public class Customer {
	private String name;
	private int point;
	
	public Customer(String name, int point) {
		this.setName(name);
		this.setPoint(point);
	}

}
