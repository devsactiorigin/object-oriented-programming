# chapter 1 Welcome to Design Pattern - intro to Design patterns
idxP 31~

## story of SimpleDuck

액면적인 요구사항에 따라 기능을 반영했지만, 이로 인한 사이드 이펙트로 다른 기능에 문제가 발생

=> 보다 치밀한 접근과 해결방식 필요

=> Design Pattern

대표적으로 슈퍼클래스의 변형과 상속을 통해서는 겉잡을 수 없는 사이드이펙트가 반복

=> 우선 적절한 인터페이스 구현해서 적용

=> 중복 코드 만큼이나 위험한, 서로 유사하지만 같지는 않은 코드로 인해서 생산성 위험

=> SOLID에 충실하자

## SOLID

SRP : 

OCP : (쉽게 설명하자면) 최소환의 변화_기능추가_로 변화에 대응

https://victorydntmd.tistory.com/291

https://m.blog.naver.com/ljh0326s/221113248565

## The Big Picture on 'Encapsulated behaviors'
idxP 52~

기존 상속트리와 별개로, 추가 요구사항에 대응할 수 있는 인터페이스나 추상클래스로 외부 구현, 하위 특정 클래스는 위의 인터페이스의 선택지를 바탕으로 탄력적으로 추가기능을 얻는다.

이 때 보다 구체적으로는, 인터페이스 또한 객체의 속성을 가지고,이 객체의 다형성을 이용한 파라미터로,

인터페이스를 상속하고, 이를 바탕으로 특정 메소드를 구현한 서브클래스를 아규먼트로 받아 추후 변화에 대응할 수 있는 추상 클래스를 설계한다.
'''

	public abstract class Duck {
		FlyBehavior flyBehavior;
		QuackBehavior quackBehavior;
	 
		public Duck() {
		}
	 
		public void setFlyBehavior (FlyBehavior fb) {
			flyBehavior = fb;
		}
	 
		public void setQuackBehavior(QuackBehavior qb) {
			quackBehavior = qb;
		}
	 
		abstract void display();
	 
		public void performFly() {
			flyBehavior.fly();
		}
	 
		public void performQuack() {
			quackBehavior.quack();
		}
	 
		public void swim() {
			System.out.println("All ducks float, even decoys!");
		}
}


'''


한편, 이러한 추상클래스를 상속하는 하위 클래스들은 생성자메소드를 통해 인터페이스 중 초기값을 선택한다.
'''

	public class ModelDuck extends Duck {
		public ModelDuck() {
			flyBehavior = new FlyNoWay();
			quackBehavior = new Quack();
		}
		...
	}

'''
