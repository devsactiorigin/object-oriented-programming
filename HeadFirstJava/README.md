# Head First Java 

## what is Java
(TO ME) Java is language of OOP

# 개요
[Intro](./src/chap00)

[01장 자바 개요](./src/chap01)

[02장 스토리 기반 객체 지향 프로그래밍 이해](./src/chap02)

[03장 변수 : Primative vs Reference](./src/chap03)

[04장 메소드와 인스턴스 변수](./src/chap04)

[05장 메소드  : 게임 프로그래밍](./src/chap05)

[06장 자바 라이브러리 : 게임 프로그래밍](./src/chap06)

[07장 상속](./src/chap07)

[08장 다형성, 인터페이스](./src/chap08) 

[09장 생성자, 가비지 컬렉션](./src/chap09)
  
[10장 Static, Wrapper Class](./src/chap10)

[11장 Exception Handling](./src/chap11)

[12장 Swing 1  -> AWT, Swing, JavaFX]()

[13장 Swing 2  -> BeatBox.java]()

[14장 I/O]()

[15장 Networking and Multithread]()

[16장 Data Structures: Collections Framework and Generics]()

[17장 Deployment]()

[18장 Distributed Computing]()

+

[ChapAppendix : related Oracle Java Tutorials](./src/chapAppendix)

+

[etc : furthermore]()