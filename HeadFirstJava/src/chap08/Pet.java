package chap08;

public interface Pet {
	public abstract void beFriendly();
	
	public abstract void play();
}
