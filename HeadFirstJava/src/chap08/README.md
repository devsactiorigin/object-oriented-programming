# Serious Polymorphism - interfaces and abstract classes

## Abstract methods and Abstract classes

'''

	public abstract void eat();

'''

body가 없어도된다. 구체 body는 상속받는 이들이 구체화한다. 한편 바디가 없을려면 abstract를 반드시 필요

한편, 특정 클래스가 개발자의 판단하에 abstract method를 설정했더라면, 그 클래스는 반드시 abstract로 설정해야 한다.


(Animal, Feline, Lion)

## Deadly Diamond of Death,  DDD

완벽한 계획은 불가능하고, 기존 상속 트리의 필드명이나 메소드를 벗어나 객체가 발생함

이를 추후 보완적으로 다중상속으로 보완가능하나, 이 또한 새로운 문제를 일으킨다.

추가 링크

https://blog.soobinpark.com/23

## Interface to the rescue

인터페이스는 다중 상속을 허락한다. 왜냐하면, 상속받는 서브클래스는 반드시 새로값을 정의해야하고 이를 통해 DDD를 피할 수 있기 때문이다.

(Animal, Feline, Lion, cat + Pet)

### 'default' with Interface
default를 붙인 메소드는 추후 요구된 메소드를 해당인터페이스에 기입하되, 이를 상속받는 클래스가 당장은 오버라이딩을 하지 않아도 되게 설정해주는 것

https://atoz-develop.tistory.com/entry/JAVA-8-interface-default-키워드와-static-메소드

## How do you know whether to make ~ ?
