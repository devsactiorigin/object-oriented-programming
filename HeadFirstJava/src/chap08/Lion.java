package chap08;

public class Lion extends Feline{
	
	// cases of overriding
	public void makeNoise() {
		System.out.print("makeNoise is overrided by Lion, and");
		System.out.println(getPictures()+" is comes from my ancestor class, Animal by getPictures()");
	}
	
	public void eat() {
		System.out.println("eat is overried by Lion");
	}
	
	// 유의! 만약에 Lion에다가 상속트리와 전혀 무관한 메소드를 추가해버리면 어떻게 될까?
	// Animal lion = new Lion();
	// 할때 lion 변수에서 Lion만의 메소드를 컨트롤할 레퍼런스가 전혀 없고, 상속트리의 의미가 퇴색 및 무력화된다.
}
