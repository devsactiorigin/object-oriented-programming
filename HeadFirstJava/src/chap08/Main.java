package chap08;
// idxP 195~
public class Main {
	public static void main(String[] args) {
		Cat cat = new Cat();
		cat.setPictures("Lion zero");
		
		System.out.print(cat.getPictures() + " class's method => ");
		cat.beFriendly();
		
		System.out.print(cat.getPictures() + " class's method => ");
		cat.play();
		
		System.out.print(cat.getPictures() + " class's method => ");
		cat.makeNoise();
	}
}
