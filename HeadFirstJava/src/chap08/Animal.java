package chap08;

public abstract class Animal {
//	picture 종, food, hunger, boundaries, location
	private String pictures;
	private String foods;
	private String hunger;
	private String boundaries;
	private String location;
	
	public String getPictures() {
		return pictures;
	}
	public void setPictures(String pictures) {
		this.pictures = pictures;
	}
	public String getFoods() {
		return foods;
	}
	public void setFoods(String foods) {
		this.foods = foods;
	}
	public String getHunger() {
		return hunger;
	}
	public void setHunger(String hunger) {
		this.hunger = hunger;
	}
	public String getBoundaries() {
		return boundaries;
	}
	public void setBoundaries(String boundaries) {
		this.boundaries = boundaries;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public abstract void makeNoise();
	
	public abstract void eat();
	
	public abstract void sleep();
	
	public abstract void roam();
}
