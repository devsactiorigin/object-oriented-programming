package chap08;

public class Cat extends Feline implements Pet{

	@Override
	public void beFriendly() {
		System.out.println("친한 척하기");
	}

	@Override
	public void play() {
		System.out.println("놀기");
	}
	
	@Override
	public void makeNoise() {
		System.out.println("makeNoise from Feline from Animal");
	}

}
