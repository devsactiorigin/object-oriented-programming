## Collections
[goto](https://docs.oracle.com/javase/tutorial/collections/index.html)

크게 collection 과 Map의 자식들로 볼수있다.

https://www.geeksforgeeks.org/data-structures/

특히 collection 계열은

add, remove, size는 모두 공통이다.

다만,

stack의 경우 부득이하게 push , pop ; 권장되는 오버라이드는 아니지만, 부득이한가봄
queue의 경우 부득이하게 offer, poll

참고로 append,pop / append, popleft

[Hierarchy of the Collection Framework](https://www.geeksforgeeks.org/collections-in-java-2/)

* 오라클의 경우 deque를 보다 넓게 collection에 묶었지만, queue 아래가 더 적절하다.

#### Array, ArrayList, LinkedList
Array vs ArrayList

ArrayList는 IO 후 새로운 배열 선언들을 통해 편의성을 증대하였지만,
 기본적으로 배열을 바탕으로 List를 구현해서, 여전히 Array 스럽게 아주 길거나 넓은 Array의 경우
 1~2개의 변경으로 인해 그 뒤나 인접의 거의 전체 원소들이 다 재이동하고, 재선언되는 문제가 발생한다.
 
LinkedList는 이러한 

ArrayList vs LinkedList

LinkedList vs vector

!! ArrayList is non-synchronized vs Vector is synchronized

if you use multithread of java, Vector is needed 'Thread safe'

나도 될수있으면 vector쓰자 단어도 짧고 좋음


#### ? Thread programming
메소드는 스택에!

그리고 main문이 스택의 맨 아래이고, 후속 호출되는 함수들, 즉 최신 함수들이 위쪽에 스택이 쌓인다.

참고로 main문은 Thread로 수행하게 설정되어있고, main 하위 메소드들을 쓰레드로 틀면,

여러개의 멀티 쓰레드가 작동중이라고 할수있다. 참고로 cpu는 쓰레드들한테 돌아가면서 작업한다.

이로 인해 자연스럽게 경합이 발생하는 중이라고도 할수있고, vector는 필수가 될수도 있다.

원리는 오라클의 locking 메카니즘과 유사, 여러가지 쓰레드들이 한가지자원을 공유할때, 생기는 무결성문제를
locking을 제공한다.

그러나 역시 vector는 동기화작업을 추가로하기 때문에 
 단일 쓰레드는 arrayList가 동기화작업이 없어서 속도 측면에서는 더 좋고 대용량이면 불가피할수도있다.


#### LinkedList와 HashSEt
오라클은 동일문장 판단에 hash를 쓰느데, 동일한 해쉬값을 가지기도하고,
같은 것 내에서는 링크드 리스트로 저장된것,

한편, 다시 이러한 자료구조를 찾을때는 같은 해쉬값 내에서는  풀스캔이 필요할수있따.
 
## Map Collection
HashMap vs TreeMap

put,get

#### 저장속도

 
## etc
TreeSet ; 정렬된 set을 위해 tree를 사용함. 한편으론 주요 연산이 '정렬'이면 Hashset보다 적합,
 hashset은 조회에 특화