package chapAppendix.languageBasics.arrays.refactoring;

/*

example In-Out

10
100 200 300 400 500 600 700 800 900 1000
-
100 200 300 400 500 600 700 800 900 1000 
[100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]

 */
import java.util.*;

public class ArrayDemo_refactoring {
    public static void main(String[] args) {
    	Scanner sc=new Scanner(System.in);
        int n= sc.nextInt();
        
    	// declares an array of integers
        int[] anArray;

        // allocates memory for 10 integers
        anArray = new int[10];
        
        for(int i=0;i<n;i++) {
        	anArray[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++) {
        	System.out.print(anArray[i]+" ");
        }
        System.out.println();
        
        System.out.println(Arrays.toString(anArray));
    }
}
