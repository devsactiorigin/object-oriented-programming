package chapAppendix.languageBasics.arrays;

import java.util.*;

class ArrayCopyOfDemo {
    public static void main(String[] args) {
        String[] copyFrom = {
            "Affogato", "Americano", "Cappuccino", "Corretto", "Cortado",   
            "Doppio", "Espresso", "Frappucino", "Freddo", "Lungo", "Macchiato",      
            "Marocchino", "Ristretto" };
        
        String[] copyTo = java.util.Arrays.copyOfRange(copyFrom, 2, 9);        
        for (String coffee : copyTo) {
            System.out.print(coffee + " ");           
        }
        
        // 추가
        String[] copyTo2 = Arrays.copyOfRange(copyFrom, 2, 9);
        for (String coffee : copyTo2) {
        	System.out.println(coffee);
        }
    }
}