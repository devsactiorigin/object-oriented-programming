/*
 * Enum은 객체이다.
 * */

package chapAppendix.EnumTypes;

public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
    THURSDAY, FRIDAY, SATURDAY 
}
