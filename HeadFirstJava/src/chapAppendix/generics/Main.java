package chapAppendix.generics;

public class Main {
	public static void main(String[] args) {
		Pair<Integer, String> p1 = new Pair<>(1, "apple");
		Pair<Integer, String> p2 = new Pair<>(2, "pear");
		boolean same = Util.<Integer, String>compare(p1, p2);
		System.out.println(same);
		
		
		System.out.println("change and compare");
		//System.out.println(p1.getValue());
		p2.setKey(1);
		p2.setValue("apple");
		System.out.println(p2.getValue());
		
		same=Util.compare(p1, p2);
		System.out.println(same);
		
        Box<Integer> integerBox = new Box<Integer>();
        integerBox.set(new Integer(10));
        //integerBox.inspect("some text"); // error: this is still String!
        integerBox.inspect(100); // error: this is still String!
		

	}
}
