package chapAppendix.nestedClasess;

public class LocalClassExample {
	// ^(Carat) means 'except ~' 0~9 사이를 빼고란 의미이다.
    static String regularExpression = "[^0-9]";
    
    public static void validatePhoneNumber(
        String phoneNumber1, String phoneNumber2) {
    	
    	// 왜 final이 붙은 걸까, 일단은 값을 capture해서 validatePhoneNumber메소드는 종료되도, 
    	// 만약 return 시 value가 사라지지 않고, capture된 값을 리턴할수있도록 하는것
        final int numberLength = 10;
        
        // 하지만 이런 겹겹히 클래스의 안정성 차원에서 8버전부터는 상관없는것으로 보임. 내가 언제꺼쓸지 모르니, 알아두자
        // 한편, 자동으로 final 이 자동 기입된다고 이해할때, final 특성대로 나중에 7이나 8로 바꿀려고 하면 에러
        // Valid in JDK 8 and later:
       
        // int numberLength = 10;
       
        class PhoneNumber {
            
            String formattedPhoneNumber = null;

            PhoneNumber(String phoneNumber){
                // numberLength = 7;
                String currentNumber = phoneNumber.replaceAll(
                  regularExpression, "");
                if (currentNumber.length() == numberLength)
                    formattedPhoneNumber = currentNumber;
                else
                    formattedPhoneNumber = null;
            }

            public String getNumber() {
                return formattedPhoneNumber;
            }
            
            // Valid in JDK 8 and later:

//            public void printOriginalNumbers() {
//                System.out.println("Original numbers are " + phoneNumber1 +
//                    " and " + phoneNumber2);
//            }
        }

        PhoneNumber myNumber1 = new PhoneNumber(phoneNumber1);
        PhoneNumber myNumber2 = new PhoneNumber(phoneNumber2);
        
        // Valid in JDK 8 and later:

//        myNumber1.printOriginalNumbers();

        if (myNumber1.getNumber() == null) 
            System.out.println("First number is invalid");
        else
            System.out.println("First number is " + myNumber1.getNumber());
        if (myNumber2.getNumber() == null)
            System.out.println("Second number is invalid");
        else
            System.out.println("Second number is " + myNumber2.getNumber());

    }

    public static void main(String... args) {
        validatePhoneNumber("123-456-7890", "456-7890");
    }
}
