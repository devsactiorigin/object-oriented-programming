package chapAppendix.nestedClasess;

public class HelloWorldAnonymousClasses {

	// 인터페이스는 static 영역에 저장된다고 일단 이해, 그래서 아래 sayHello에서 바로 implement못함, non static이라
	interface HelloWorld {
		public void greet();

		public void greetSomeone(String someone);
	}

	public void sayHello() {

		class EnglishGreeting implements HelloWorld {
			String name = "world";

			public void greet() {
				greetSomeone("world");
			}

			public void greetSomeone(String someone) {
				name = someone;
				System.out.println("Hello " + name);
			}
		}

		// std : 일반적인 유명 클래스(not Anonymous class), it name is 'englishGreeting'
		HelloWorld englishGreeting = new EnglishGreeting();
		// experi : anonymous 방식의 무명 클래스(anonymous class)
		HelloWorld frenchGreeting = new HelloWorld() {
			String name = "tout le monde";

			public void greet() {
				greetSomeone("tout le monde");
			}

			public void greetSomeone(String someone) {
				name = someone;
				System.out.println("Salut " + name);
			}
		};
		
		// 인터페이스 블록인데...내용이 들어가있음. 그리고 신기하게 ;으로 끝남 마치 1개의 메소드처럼, 이것이 anonymous class expression
		// 다시 돌아가자면, 인터페이스로 객체를 만들수있나? 없다!
		// 아래의 의미는 HelloWorld라는 인터페이스를 상속하는 '익명의 클래스'를 new하고
		// 그 내용을 채우는 내용 방식의 코딩이다!
		HelloWorld spanishGreeting = new HelloWorld() {
			String name = "mundo";

			public void greet() {
				greetSomeone("mundo");
			}

			public void greetSomeone(String someone) {
				name = someone;
				System.out.println("Hola, " + name);
			}
		};
		englishGreeting.greet();
		frenchGreeting.greetSomeone("Fred");
		spanishGreeting.greet();
	}

	public static void main(String... args) {
		HelloWorldAnonymousClasses myApp = new HelloWorldAnonymousClasses();
		myApp.sayHello();
	}
}
