# summary of chapter Appendix 2
  https://docs.oracle.com/javase/tutorial/java/javaOO/nested.html

  - Static nested class

  - Non-static nested class = Inner Class
    - 일반적인 Inner Class
    - (Named) Local Class 
    - Anonymous (Local) Class

  - Lambda Expressions 

    - A functional interface is any interface that contains only one abstract method.

    - https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
    - https://codechacha.com/ko/java8-functional-interface/   
    - Function 함수적 인터페이스 : Function 함수적 인터페이스 예제.txt
    - Java AndThen, Compose 사용 : https://moreget.github.io/dev-00000064-Java-ConsumerAndThen/

    - Consumer
    - Supplier
    - Function
    - Operator
    - Predicate
    - ...

  - Method Reference : https://www.javatpoint.com/java-8-method-reference

  - When to Use

# Nested Classes

## Why Use Nested Classes?
특정 클래스가 다른 특정클래스만을 위해 필요하다면, 즉 호출횟수가 몇번안된다면, 별도로 독립시키기보단 내재화하는것이
가독성이나 구조 상 강력히 추천되는 것으로 이해
## Inner Classes

## Shadowing
(ShadowTest.java)
## Serialization
직렬화는 매우 비권장됨

## Inner Class Example
(DataStructure.java)

## Local Classes
https://docs.oracle.com/javase/tutorial/java/javaOO/localclasses.html

(LocalClassExample.java)

## Anonymous Classes
https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html

(CustomTextFieldSample.java)

## Lambda Expressions
https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html

```
!! Core
Lambda expressions let you express instances of single-method classes more compactly.

```

plus references

https://codechacha.com/ko/java8-functional-interface/

참고로, 

```
public interface Supplier<T> {
    T get();
}
```
와같은 곳에서 return도 생략가능한가봄

