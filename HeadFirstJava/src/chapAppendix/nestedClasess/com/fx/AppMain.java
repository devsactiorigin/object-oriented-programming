package chapAppendix.nestedClasess.com.fx;

/*

독립 프로젝트로 생성 시 ; JavaFX100

다만, 현재의 경우 프로젝트명은 HeadFirstJava이고, 프로젝트 환경설정의 기준은 HeadFirstJava를 기준으로 합니다.
그리고 이 프로젝트 하위에 Lib라는 폴더에 커스텀 lib들을 모아두기로 하였다.

처음 기입 시, 필요한 Java ARchive가 없기때문에 에러문이 뜸
=> 필요한 Jar를 저장한다.r

  - 프로젝트 환경설정 : jar 파일 등록

    C:\Program Files\Java\jdk1.8.0_301\jre\lib\ext\jfxrt.jar
    의 파일을 상단 Lib 폴더 아래에 배치하고
    
    HeadFirstJava prj를 우클릭하고, properties 클릭, Java Build Path 클릭, Libraries 탭클릭, Add Jar 클릭해서 Lib하위의 해당 Jar클릭 후 Apply

	그러면 'Referenced Libraries가 생긴다.

  - JRE 설정

    -> Eclipse 상단 메뉴창의 Window에서 Preferences를 열어 Java | Compiler를 찾아 Compiler compliance level을 1.8로 설정합니다.

    -> Eclipse Preferences를 열어 Java | Installed JREs를 찾습니다.
    -> *Add..*를 클릭한 후 Standard VM 선택한 뒤 JDK 8이 설치되어 있는 디렉토리를 고릅니다.
    
    	C:\Program Files\Java\jdk1.8.0_301 경로로 들어감
    
    -> 다른 JRE나 JDK를 삭제합니다. 그러면 JDK 8 하나만 남습니다.

  - 이클립스에 e(fx)clipse 설치 : Eclipse Marketplace에서 검색해서 설치
 */

import javafx.application.Application;
import javafx.stage.Stage;

public class AppMain extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.show();  //윈도우 보여주기
	}
	
	public static void main(String[] args) {
		launch(args); //AppMain 객체 생성 및 메인 윈도우 생성
	}
}