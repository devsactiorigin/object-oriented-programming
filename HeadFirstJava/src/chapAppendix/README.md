# chapter Appendix : Oracle Java Tutorials
[goto](https://docs.oracle.com/javase/tutorial/index.html)

the related with chap of Head First Java, and Extensions

## Learning the Java Language: Table of Contents
[goto](https://docs.oracle.com/javase/tutorial/java/TOC.html)

### Object-Oriented Programming Concepts

#### What Is a Class? -> chap 00
https://docs.oracle.com/javase/tutorial/java/concepts/index.html

(Bicycle,BicycleDemo)

#### What Is Inheritance? -> chap 07
https://docs.oracle.com/javase/tutorial/java/concepts/index.html

#### What Is an Interface? -> chap 08
https://docs.oracle.com/javase/tutorial/java/concepts/interface.html
  
#### Math -> chap 10
https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html

###Classes and Objects

#### Enum Types

# Extensions

## Generic
https://docs.oracle.com/javase/tutorial/java/generics/index.html

## Nested Classes
[goto](https://docs.oracle.com/javase/tutorial/java/javaOO/nested.html)

#### Anonymous Classes
[goto](https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html)

#### Method reference
[goto](https://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html)

## Collections
[goto](https://docs.oracle.com/javase/tutorial/collections/index.html)

#### ArrayList, LinkedList
ArrayList는 IO 후 새로운 배열 선언들을 통해 편의성을 증대하였지만,
 기본적으로 배열을 바탕으로 List를 구현해서, 여전히 Array 스럽게 아주 길거나 넓은 Array의 경우
 1~2개의 변경으로 인해 그 뒤나 인접의 거의 전체 원소들이 다 재이동하고, 재선언되는 문제가 발생한다.
 
LinkedList는 이러한 

#### Aggregate Operations
[goto](https://docs.oracle.com/javase/tutorial/collections/streams/index.html)

point1.
Aggregate operation에 Stream 메소드가 있는거지, 참고로 파일 입출력 시 Stream과는 기원이 다르다. 그러나 구분하지 않아 기원이 같은 것으로 혼당하기 쉽다. 결론 ; aggregate operation vs stream

point2. 

