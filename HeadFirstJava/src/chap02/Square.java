package chap02;

// extends : 꼭 부모자식뿐만 아니라, 앞선 무언가를 승계하여 작업하는 '시작점부터 다른 곳으로의 연장, 시작점에서 주변으로의 확장'으로 이해하자.
public class Square extends Shape {
	// 이 또한 아래와같이 '클래스 명(Shape)'와 동일한 이름의 메소드, 즉 생성자 메소드가 자동지원된다고 볼수있다.
	// 정리하자면, Shape는 Object를 상속받고, Square는 Shape를 상속받아 일종에 껍질이 2개인 Object(객체)로 이해가능하다.	
//	public Square() {
//		super();
//	}
	
	// 위와 같은 맥락에서, 드라이버 클래스에서 square가 생성되고, rotate가 호출되면, 첫번째로 Object에 이러한 메소드가 있는지, 그리고 Shape에 있는지 체크하면서
	// 최종적으로 Square의 rotate가 호출된다.	
	// ?! 어떻게 shape의 rotate와 Square의 rotate를 선별할까? 개인적으로 현재 클래스명을 구분해서 선택실행하면 될듯하다, 또한 탐색과정도 겉껍질부터하면되기도 하고

	void rotate() {
		System.out.println("Square 빙글빙글");
	}
	
	void playSound() {
		System.out.println("Square AIF 사운드");
	}
}
