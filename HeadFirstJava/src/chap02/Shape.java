package chap02;

public abstract class Shape {
	// 참고로 Shape 클래스는 extends Object가 자동 지원된다고 볼 수 있다.
	
	// 또한 아래와같이 '클래스 명(Shape)'와 동일한 이름의 메소드, 즉 생성자 메소드가 자동지원된다고 볼수있다.
//	public Shape() {
//		super();
//	}
	
	void rotate() {
		System.out.println("빙글빙글");
	}
	
	void playSound() {
		System.out.println("AIF 사운드");
	}
	

}
