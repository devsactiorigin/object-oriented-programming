package chap02.GuessGame;
// OOP 개발에 있어, superclass 부터 subclasses 로 내려가거나 그 반대도 모두 허용가능하다.
// 전자의 경우, 스토리를 먼저 짜면서 그때그때 대응하는 클래스를 선언하고 추후 채워나가는 방식이다.
public class GameLauncher {
    public static void main (String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
