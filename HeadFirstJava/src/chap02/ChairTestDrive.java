package chap02;

public class ChairTestDrive {
	// remind ! we can call this class 'ChairTestDrive' is Driver class that drives other classes(or ~.java files)
	public static void main(String[] args) {
		// 아래와 같이 Shape가 new로서 인스턴스화 되었다. 이것의 형체는? ...알수없다
		// => abstract 키워드를 통해서 이와같은 OOP의 혼란을 해소, 상속 트리에서 abstract한 부분들은 new를 못하게 제어
//		Shape shape = new Shape();
//		shape.rotate();
//		shape.playSound();
		
		Square square = new Square();
		square.rotate();
		square.playSound();
		
		Amoeba amoeba = new Amoeba();
		amoeba.rotate();
		amoeba.playSound();
			
	}
}
