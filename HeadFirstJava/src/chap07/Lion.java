package chap07;

public class Lion extends Feline{
	
	// cases of overriding
	public void makeNoise() {
		System.out.print("makeNoise is overrided by Lion, and");
		System.out.println(getPictures()+" is comes from my ancestor class, Animal by getPictures()");
	}
	
	public void eat() {
		System.out.println("eat is overried by Lion");
	}

}
