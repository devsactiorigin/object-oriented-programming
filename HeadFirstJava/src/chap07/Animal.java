package chap07;

public abstract class Animal {
//	picture 종, food, hunger, boundaries, location
	private String pictures;
	private String foods;
	private String hunger;
	private String boundaries;
	private String location;
	
	public String getPictures() {
		return pictures;
	}
	public void setPictures(String pictures) {
		this.pictures = pictures;
	}
	public String getFoods() {
		return foods;
	}
	public void setFoods(String foods) {
		this.foods = foods;
	}
	public String getHunger() {
		return hunger;
	}
	public void setHunger(String hunger) {
		this.hunger = hunger;
	}
	public String getBoundaries() {
		return boundaries;
	}
	public void setBoundaries(String boundaries) {
		this.boundaries = boundaries;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void makeNoise() {
		System.out.println("i'm primary makeNoise of Animal");
	}
	
	public void eat() {
		System.out.println("i'm primary eat of Animal");
	}
	
	public void sleep() {
		System.out.println("i'm primary sleep of Animal");
	}
	
	public void roam() {
		System.out.println("i'm primary roam of Animal");
	}
}
