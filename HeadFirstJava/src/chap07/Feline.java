package chap07;

public abstract class Feline extends Animal{
	
	// case of overriding
	public void roam() {
		System.out.println("roam is overrided by Feline, it will affect my descend of Inheritance Tree");
	}

}
