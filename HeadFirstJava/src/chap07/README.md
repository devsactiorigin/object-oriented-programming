# Better Living in objectville - inheritance and polymorphism
!! Inheritance is completed by Overriding

let's assume that wolf extends Canine, Canine extends Animal

and wolf can overrides methods of Animal, of Course Canine. and if needed, can use superclass's fields by 'super.~;'

Meanwhile, if you definite like

Animal wolf = new Wolf();

it is possible, but you cannot use method of wolf. animal can involve Animal, but reverse cannot

## Impl of Inheritance

Animal, Feline,Lion, Main

## Using IS-A and HAS-A
sub IS-A super

IS-A is a virtual tool to check the Inheritance Tree

## So what does all this inheritance really buy you?

you avoid duplicate code

you define a common protocol for a group of classes.(superclass's feature operates as a protocol)

## With ploymorphsim, the reference type can be a superclass of the actual object type

I can make Array that has flexible objects

'''

	Animal[] animals = new Animal[5];
	
	animal[0] = new Dog();
	animal[1] = new Cat();
	animal[2] = new Wolf();
	animal[3] = new Hippo();
	animal[4] = new Lion();


'''

You can have polymorphic arguments and return types
'''

	class Vet{
		public void giveShot(Animal a){
			a.makeNoise();
		}
	}

'''

## Overloading a method

overlo---------ading is horizontal conception, overrIIIIIIIIIding is vertical

오버로오오오오오오딩은 상속이랑 전혀 무관하다. 단순히 리턴값에 의해 여러 개의 동일명 함수가 한 클래스 안에 존재한다고 이해가능