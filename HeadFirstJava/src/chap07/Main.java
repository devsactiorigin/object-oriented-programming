package chap07;
// idxP 195~
public class Main {
	public static void main(String[] args) {
		Lion lion = new Lion();
		lion.setPictures("Lion zero");
		
		System.out.print(lion.getPictures() + " class's method => ");
		lion.makeNoise();
		
		System.out.print(lion.getPictures() + " class's method => ");
		lion.eat();
		
		System.out.print(lion.getPictures() + " class's method => ");
		lion.roam();
	}
}
