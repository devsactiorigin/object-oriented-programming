package chap00;


/*
 * https://docs.oracle.com/javase/tutorial/getStarted/intro/definition.html
 * 
 * lesson1 : statements line 
 * 
 * lesson1 : public VS private
 * 
 * lesson2 : static
 * 우선 static이 붙은 변수는 BicycleDemo에서는 별도의 new Bicycle();없이도 변수가 할당가능하다
 * 개인적으로 전역변수화 되서 이미 메모리화 된것으로 추정
 * 
 * ? 왜 public static void main에서 static 이 붙는 것일까?
 * 우선 편리해서, 그리고 교수님 설명에 의하면, 이해를 잘못했지만, 인스턴스가 new를 통해서 메모리화 되는 그 재귀구조의 시작점을 짚어주는 것으로 보인다.
 * 그 결과, 개발자는 BicycleDemo main문에서 new Bicycle(); 없이도 바로 호출해서 사용가능하게 된다고 한다.
 * 
 * 한편, main문도 결국은 실행공간으로 전달되서 실행되는 곳이 별도인데, 개발자가 new main();같은 과정없이 실행할 수 있는 배경으로 일단 이해
 * 
 */

public class Bicycle {
    // fields or after new cmd, it canbe called instance variables
    // futhermore, if this class is part of API of JAVA-APP, and this API transfers state of instance of Bicycle REprensentatively, 
    // we can call it as "RestFul"
    
    int cadence = 0;
    int speed = 0;
    int gear = 1;

    // plus, based on same Class, but if you insert the important field that can definite the sub Category of Class like 'int typeBicycle'// 1 MTV, 2 road ...
    // we can say the mutiple variations can comes from class
    void changeCadence(int newValue) {
         cadence = newValue;
    }

    void changeGear(int newValue) {
         gear = newValue;
    }

    void speedUp(int increment) {
         speed = speed + increment;   
    }

    void applyBrakes(int decrement) {
         speed = speed - decrement;
    }
    
    // 아래와 같은 함수는 현재 클래스의 필드값과 무관하므로, 추후 정규화과정에서 삭제되야할 부분이다.
//    void printName() {
//    	System.out.println("Who am i?");
//    }

    void printStates() {
         System.out.println("cadence:" +
              cadence + " speed:" + 
              speed + " gear:" + gear);
    }
}
