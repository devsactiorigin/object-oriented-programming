/*
 * lesson1 : eclipse shortcut
 * main & (ctrl & spacebar) => 메인문 자동생성
 * syso & (ctrl & spacebar) => println 자동생성
 * 
 * () : parenthesis
 * {} : brace
 * [] : bracket
 * < > : angle bracket
 * 
 */

package chap00;

public class MyFirstApp {
	public static void main(String[] args) {
		System.out.println();
	}
}
