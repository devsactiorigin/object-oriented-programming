# chapter 10 : Numbers Matter - numbers and statics

## Math method
참고로 Math class는 싱글턴 패턴으로 생성자가 private로 설정되어 외부클래스에서 사용 시, new Math(); 방식으로 사용불가능하다.

## The difference between regular(non-static) and static methods

check point ! static 메소드는 static 메소드나 필드만 접근가능하다! 그렇게 설계되었다

한편, Math 클래스의 new없이 바로 이 클래스의 하위메소드를 호출가능했다. 우선은 new에 대응하는 무언가가 존재하고,

어쩌면 이미 new 되어있고, static은 공유자원으로서 존재중인게 아닐까? 그래서 바로 접근가능한거아닐까?

## Initializing a static variable

## static final variables are constants

constants vs literal

둘 다 그릇에 비유하자면, 전자는 뚜겅을 닫아놔서 변경이 불가능하다. 후자는 가능하다.

'''
	
	public static final int FOO_x =25; // ~ 'public final' 변수는 반드시 초기화시켜야 컴파일 에러가 안뜬다.
	
'''

한편, final만 붙어있으면 최초의 1회만 변경가능한 것이다.

## Wrapping a primitive

대표적으로 Collections에서는 primitive type을 따로 받지 않고, '객체'단위로 받게 설계되었고,

이를 위해서 중간에 랩핑할 것이 필요한데, 이것을 지원해주는 Wrapper class이다.

!! 그러나, 자바 버전 5.0 이상부터는 이를 자동지원해준다! => auto Boxing !!

## Number formatting

check points ; String.format(~), Calendar.getIntance()