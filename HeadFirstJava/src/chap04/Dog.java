package chap04;

public class Dog {
	private int privatedSize;
	private String privatedName;
	
	int defaultSize;
	String defaultName;
	
	int size;
	String name;
	// 의도적으로 허용된 메소드이므로 public 필수
	// 파라미터 명과 필드명이 다른 경우는 문제없지만,
	// 개인적으로 이러한 별도의 파라미터값을 새로 매번 적절히 지어내는 것이 쉽지 않을 것으로 예상되고,
	// 필드명과 동일한 파라미터명을 쓰되, 그것을 구붆하기 위해 아래 setSize에서 this를 활용
	public void testsetSize(int amu) {
		size=amu;
	}

	public void setSize(int size) {
		this.size=size;
	}

	
	public int getSize() {
		return size;
	}
}
