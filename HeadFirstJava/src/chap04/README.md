# chapter 4 : How Objects Behave - methods use Instance variables
Remember: a class describes what an object knows and what an object does

## Encapsulation
(To ME) 객체 개념을 보존하면서 인스턴스를 구분하기 위한 것

실현방법 : 보존하고자 하는 필드값 변수 앞에 private

### getter,setter : Accessor || Mutators

실습자료 ; chap04 Dog, DogTestDrive

Encapsulation을 바탕으로 인스턴스의 필드값을 적절히 조절하기 위한 메소드

!! 이 경우, 의도적으로 외부에서 접근가능한 길을 만들기 위해 만든 메소드이기 때문에 getter,setter의 접근지정자를 private로 해서 보이지 않게 설정하면 안된다.

!! lombok을 통해 access muator 코딩과정을 자동지원받을 수있다.


## You can Send things to a method

A method uses parameters. A caller passes arguments (to method of class)
* 매개 변수,인자 ; formal parameter - actual parameter in oracle


## Java is pass by value ; call by value in perspective of computer

reference variable이 약간 call by reference 느낌이 나지만 python의 shellow copy 문제는 없는 것으로 안다.

## Delcaring and initailizing instance variables

poorDog라는 클래스의 필드값이 초기화되지 않은 상태에서

set없이, get을 바로하면 어떤값이 출력될까? 0, null 등이 나온다. 마치 그렇게 선언된것처럼.

이러한 기능이 자동지원된다고 한다.

## Comparing variables (primitives or references)

