package chap04;

public class DogTestDrive {
	public static void main(String[] args) {
		// 실험 상황, 같은 패키지 내의 Dog 클래스를 선언하는데, 그 필드값은 private_size, name_하거나 default_size2,name2_로 설정되어있다.
		Dog one = new Dog();
		
		// 접근지정자가 default로 설정된 필드값들
		one.defaultSize=1;
		one.defaultName="i'm name2";
		
		System.out.println(one.defaultSize);
		System.out.println(one.defaultName);
		
		// 접근지정자가 private로 설정된 필드값들을 제어하려고 하면

//		one.size=2;
//		System.out.println(one.size);

//		Exception in thread "main" java.lang.Error: Unresolved compilation problems: 
//			The field Dog.size is not visible
//			The field Dog.size is not visible
//
//			at chap04.DogTestDrive.main(DogTestDrive.java:15)
		
		// 위와같은 에러문 발생
		
		
		// Access mutator
		one.setSize(100);
		System.out.println(one.getSize());
	}
}
