package chap11;

public class CheckingExceptions {
	// 설계자는 아래와같이 미리 짜놔야하고,
	static void checkAge(int age) throws ArithmeticException{
	    if (age < 18) {
	      throw new ArithmeticException("<ck>Access denied - You must be at least 18 years old.");
	    }
	    else {
	      System.out.println("Access granted - You are old enough!");
	    }
	  }

		// 사용자가 어떻게 이것을 쓸지로 볼 수 있음
	  public static void main(String[] args) {
		  try {
			  checkAge(15); // Set age to 15 (which is below 18...)
		  } catch(ArithmeticException ex) {
			  System.out.println(ex.getMessage());
		  }
		  
	  }
}
