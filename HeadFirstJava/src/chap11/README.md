# chap11 Risky Behavior - exception handling

## First we need a Sequencer

우선 개발자가 예외처리를 포함한 클래스를 설정하고, 사용자도 try-catch 등을 통해 처리해야한다. 한편, throws인 이유는 1 클래스 당 에외를 여러개 설정할 수 있기 때문이다. 그러면 catch가 추가된다.

try 안에는 에러발생 위험이 있는 메소드를 시도하고, 이에 대해서 예외발생 시 catch가 작동하므로 대응 안내문이나 별도 설정

참고로, 예외처리는 실무 서비스에서 필수요소이고, 예외처리가 원만히 처리된다면, 이또한 성공프로세스이다.

## Riksy, exception-throwing code


## The complier checks for everything except RuntimeExceptions.
compiler는 syntax error 만 잡는다. 나머지 런타임에러는 우선 수행해보고 난뒤 체크가능하다.(가령 무한루프 등)

제너럴한 예외에 대해선 기존의 Exception ex를 사용할수있으나, 모호해진다.

## Exceptions Rules
!! 참고링크 반드시 체크
