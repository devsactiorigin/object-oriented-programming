package chap05;

// 동일 폴더에 배치했으므로 주석처리
//import helpers.GameHelper;
import java.util.Scanner;

public class Game {
    public static void main(String[] args)
    {
        int numOfGuesses = 0;
        GameHelper helper = new GameHelper();
        
        SimpleDotCom theDotCom = new SimpleDotCom();
        int randomNum = (int) (Math.random() * 5);
        // 아까와달리 랜덤하게 설정
        int[] locations = {randomNum, randomNum+1, randomNum+2};
        theDotCom.setLocationCells(locations);
        
        boolean isAlive = true;
        while (isAlive == true)
        {
            String guess = helper.getUserInput("enter a number");
            String result = theDotCom.checkYourself(guess);
            numOfGuesses++;
            // 다만, 아직 1개의 hit 값을 연속으로 넣어도 kill되서 게임이 종료됨, 
            // 원래라면 hit를 출력할 3개이상의 값들이 모두 들어가야한다.
            if (result.equals("kill")) {
                isAlive = false;
                System.out.println("You took " + numOfGuesses + " guesses");
            }
        }
    }
}
