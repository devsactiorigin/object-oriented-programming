package chap05;

public class SimpleDotComTester {
    public static void main(String[] args)
    {
        SimpleDotCom dot = new SimpleDotCom();
        int[] locations = {2, 3, 4};
        dot.setLocationCells(locations);
        // 위 locations 배열의 값 중 하나를 userGuess로 설정하며 hit, 아니면 miss
        String userGuess = "2";
        String result = dot.checkYourself(userGuess);
    }
}
