package chap05;

public class SimpleDotCom {
    int[] locationCells;
    int numOfHits = 0;
    
    public void setLocationCells(int[] locs)
    {
        locationCells = locs;
    }
    
    public String checkYourself(String stringGuess) {
    	// String으로 들어온 파라미터를 Int로
        int guess = Integer.parseInt(stringGuess);
        String result = "miss";
        for (int cell: locationCells)
        {
            if (guess == cell) {
                result = "hit";
                numOfHits++;
                break;
            }
        }
        // 최초 의도한 바는 초기설정된 hit values 3개, 가령 2,3,4,를 모두 입력받았을때 kill한다임
        // 그러나 2를 3번 연속 기입시 kill되는 버그가 발생하기도 한다.
        // chap06 확인
        if (numOfHits == locationCells.length)
        {
            result = "kill";
        }
       
        System.out.println(result);
        return result;
    }
}
