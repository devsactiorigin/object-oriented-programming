# chapter 5 : Extra-Strength Methods - writing a program
chap05,06은 한가지 스토리로서 05에서 어려운부분을 06에서 ArrayList로 해결

## Developing a Class
prepare code ; a form of pseudo code

test code ; test scenario

read code ; real code

=> SimpleDotCom and SimpleDotComTester => Game, GameHelper

## Casting

자료형 데이터 크기별 차이로 인한 데이터 유실을 체크한다.

지원되는 자동전환 외에도, Integer 를 이용한 의식적 전환을 체크한다.