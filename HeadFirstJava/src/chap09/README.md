# chapter 9 : Life and Death of an Object - constructors and garbage collection
(To ME)Method들은 Stack, Object들은 Heap

JVM은 OS로부터 자원을 할당받은 뒤, 마치 자신이 OS인 것처럼 프로세스를 정의하고 처리한다고 가정할때,

이제부터 JVM은 OS로 간주할 수 있다. OS는 프로세스를 단위로 작업을 처리한다.

 그리고 익히 알려진 바로, 프로세스는 Code/Data/Heap + Stack으로 구분 가능하고
 
 프로세스를 다른 관점에서 분석하자면, 공유자원(Code/Data/Heap)+Thread인데, 이때 공유자원에는 Heap이 들어있고, Thread 안에는 Stack이 존재한다.
 
 이러한 구조에서 JVM의 Stack에는 메소드들이 저장, Heap에는 객체들이 저장되며, 객체들이 존재하는 Heap은 공유자원으로서 모든 스레드가 접근가능하다. 그리고, 힙 영역은 GC의 관리대상으로서 더이상 쓰이지 않은 객체는 삭제된다.

[프로세스와 스레드의 차이](https://velog.io/@raejoonee/%ED%94%84%EB%A1%9C%EC%84%B8%EC%8A%A4%EC%99%80-%EC%8A%A4%EB%A0%88%EB%93%9C%EC%9D%98-%EC%B0%A8%EC%9D%B4)

한편, 변수에는 Instance Variable(필드 변수)와 Local Variable(지역변수)가 존재하는데, 'Method들은 스택'이라는 점에서 지역변수는 메소드 안에 있으므로, Stack에 저장되고, 필드 변수는 힙에 저장된다.

메소드 안에 있는 객체변수는 어디에 저장될까? "Object들은 Heap"이지, reference of Object들은 아니므로,
스택에 저장

```
No matter WHERE the object reference variable is declared,...
the object always always always goes on the heap.
```

## Methods are stacked

연쇄적인 호출 시, 마지막 즉 최근 메소드가 제일 호출되고 실행될수있도록 스택형태로 스택프레임을 쌓는것


## plus Static variables
(아마도) static 변수나 메소드는 프로세스의 Data 섹션에 저장되고 이는 공유자원으로서 1프로세스 동안 유지되어, 지역변수와 달리 누적성을 가진다고 일단 이해

[static](https://devlog-wjdrbs96.tistory.com/34)

## The miracle of object creation

constructor

### Superclass Constructor with arguments

point 1 상위 클래스의 생성자와 자신의 생성자를 동시에 호출 불가